""" KNXALEXA by Louis Markert
    read  http://www.instructables.com/id/Hacking-the-Amazon-Echo/  for more information
"""
import time
import fauxmo
import logging
import time
import requests

from debounce_handler import debounce_handler

logging.basicConfig(level=logging.DEBUG)


class device_handler(debounce_handler):
    """Publishes the on/off state requested,
       and the IP address of the Echo making the request.
    """
    TRIGGERS = { "Example": 52001}

    def act(self, client_address, state, name):
        print "State", state, "on ", name, "from client @", client_address
	if name == "Example" and  state == True:
                r = requests.get("http://localhost/knx.php?adr=1/1/1&pwr=1")
        if name == "Example" and state == False:
                r = requests.get("http://localhost/knx.php?adr=1/1/1&pwr=0")

	return True

if __name__ == "__main__":
    # Startup the fauxmo server
    fauxmo.DEBUG = True
    p = fauxmo.poller()
    u = fauxmo.upnp_broadcast_responder()
    u.init_socket()
    p.add(u)

    # Register the device callback as a fauxmo handler
    d = device_handler()
    for trig, port in d.TRIGGERS.items():
        fauxmo.fauxmo(trig, u, p, None, port, d)

    # Loop and poll for incoming Echo requests
    logging.debug("Entering fauxmo polling loop")
    while True:
        try:
            # Allow time for a ctrl-c to stop the process
            p.poll(100)
            time.sleep(0.1)
        except Exception, e:
            logging.critical("Critical exception: " + str(e))
            break
